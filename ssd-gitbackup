#!/bin/bash
GBRoot="$1"; shift
MonDir="$1"; shift

renice 5 -p $$ &>/dev/null

abspath() {
	# Yes, this is a bit ugly :/
	echo "$(cd "$(dirname "$1")" && echo "$PWD")/$(basename "$1")"
}

scriptDir="$(abspath "$(dirname "$0")")"

if [[ -z "$MonDir" ]]; then
	if [[ -z "$GBRoot" && -d "${scriptDir}/gitrepo" ]]; then
		if [[
			-d "${scriptDir}/master"
		||
			"${scriptDir:${#scriptDir}-3}" == '.gb'
		]]; then
			GBRoot="${scriptDir}"
		fi
	fi
	if [ -d "${GBRoot}/master" ]; then
		MonDir="${GBRoot}/master"
	elif [ "${GBRoot:${#GBRoot}-3}" == '.gb' ]; then
		MonDir="${GBRoot:0:${#GBRoot}-3}"
	else
		GBRoot="$(abspath "${GBRoot}")"
		if [ -d "${GBRoot}.gb" ]; then
			# Assume MonDir was provided alone... 
			MonDir="$GBRoot"
			GBRoot="${MonDir}.gb"
		fi
	fi
if [ -z "$MonDir" ]; then
		echo "error: neither gitbackup root nor monitor directory were provided" >&2
	exit 19
	fi
fi

if [ -z "$GBRoot" ]; then
	while [ "${MonDir:${#MonDir}-1}" == "/" ]; do
		MonDir="${MonDir:0:${#MonDir}-1}"
	done
	GBRoot="${MonDir}.gb"
fi

if ! [ -d "$GBRoot" ]; then
	echo "error: $GBRoot is not a directory" >&2
	exit 18
fi

[ -z "$GITBACKUP" ] && GITBACKUP="$(PATH="${GBRoot}:${scriptDir}:$PATH" which gitbackup)"
[ -z "$GITBACKUP" ] && exit 16  # Message provided by 'which'
PIDFILE="${GBRoot}/gitbackup.pid"
GIT_DIR="${GBRoot}/gitrepo"
if ! [ -d "$GIT_DIR" ]; then
	echo "error: $GIT_DIR is not a directory" >&2
	exit 17
fi

PIDFILE="$(abspath "${PIDFILE}")"
MonDir="$(abspath "$MonDir")"
GIT_DIR="$(abspath "$GIT_DIR")"

if [ -e "$PIDFILE" ]; then
	if ps -p "$(<"$PIDFILE")" -o args= e |
	   grep -q " GIT_DIR=$GIT_DIR\( \|$\)"; then
		# Already running...
		exit 0
	fi
fi

LogFile="${GBRoot}/gitbackup.log"
if [ -L "$LogFile" ] || ! [ -w "$LogFile" ]; then
	LogFileBase="$LogFile"
	LogFile="${LogFile}.$(date +%F)"
	LFsuffix=
	while [ -e "$LogFile$LFsuffix" ]; do
		let LFsuffix="${LFsuffix:1}"+1
		LFsuffix=".$LFsuffix"
	done
	LogFile="$LogFile$LFsuffix"
	[[ -L "$LogFileBase" || ! -e "$LogFileBase" ]] &&
		ln -sf "$(basename "$LogFile")" "$LogFileBase"
fi

{
	exec >>"$LogFile" 2>&1
	export GIT_DIR
	cd "${MonDir}"
	exec \
	"${GITBACKUP}" .
} &
echo "$!" >"${PIDFILE}"
echo "Started gitbackup on PID $!"

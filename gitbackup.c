#define CHECKS
#define DEBUG
#define VERBOSE
//#define DRY_RUN
#define IGNORE_ROOT_GIT

#define MAX_ARGS 50


#define _BSD_SOURCE
#define _XOPEN_SOURCE 500

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <dirent.h>

/* When to commit:
 * - Every change
 * - Every N seconds / cron
 * - N seconds after changes 'settle'
 * Never wait longer than N seconds from a change to commit
 * 
 * For each of N repositories...
 * When to push:
 * - Every commit
 * - Every N seconds / cron
 * - N seconds after changes 'settle'
 * - N seconds after commits 'settle'
 * Never wait longer than N seconds from a (change | commit) to push
 * 
 * TODO: resume counter-acted schedule based on original start time?
 */

#ifndef PATH_MAX
#	define PATH_MAX 512
#endif
#define BUFFER_SIZE (sizeof(struct inotify_event) + PATH_MAX) * 10

#ifdef DEBUG
#	define LOG_DEBUG(fmt, stuff...) fprintf(stderr, fmt "\n", stuff)
#else
#	define LOG_DEBUG(...) 0
#endif

#define MY_WATCH_EVENTS (IN_CREATE | IN_DELETE | IN_DELETE_SELF | IN_MODIFY | IN_MOVED_FROM | IN_MOVED_TO | IN_Q_OVERFLOW)

static
int
read_will_block(int fd) {
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(fd, &fds);
	
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 0;
	
	return !select(fd + 1, &fds, NULL, NULL, &tv);
}

static
int
my_system2(const char*fp, char *const argv[]) {
#if defined(VERBOSE) || defined(DEBUG)
#ifdef DEBUG
	printf("exec: %s\n", fp);
	for (int i = 1; argv[i]; ++i)
		printf("  argv[%d]: %s\n", i, argv[i]);
#else
	printf("'%s'", fp);
	for (int i = 1; argv[i]; ++i)
		printf(" '%s'", argv[i]);
	puts("");
#endif
#endif
#ifdef DRY_RUN
	return 0;
#endif
	
	{
	pid_t pid = fork();
	if (pid)
	{
		// Parent
		if (pid == -1)
		{
			perror("fork");
			abort();
			return -1;
		}
		int status;
		waitpid(pid, &status, 0);
		return WEXITSTATUS(status);
	}
	}
	
	// Child
	int (*f_exec)(const char *, char *const argv[]);
	f_exec = (fp[0] == '/') ? execv : execvp;
	
	f_exec(fp, argv);
	perror("exec");
	exit(127);
}
#define my_system(f, argv...) my_system2(f, (char *const[]){f, argv, NULL})

enum action {
	A_ADD,
	A_RM,
	A_REPLACE,
	A_MODIFY,
	A_NONE = -1,
};

struct vll {
	char*p;
	enum action action;
	struct vll*next;
	uint32_t flags;
};

static
void
vll_set_action(struct vll**top, const char*p, enum action action, uint32_t flags) {
	for (struct vll *this, **thisp = top; this = *thisp; thisp = &this->next)
	{
		if (!strcmp(this->p, p))
		{
#ifdef DEBUG
			enum action actIn = action;
#endif
			switch ((this->action << 8) | action) {
			case (A_RM      << 8) + A_ADD:
				action = A_REPLACE;
				break;
			case (A_ADD     << 8) + A_RM:
				// Delete the plan to add
				action = -1;
				break;
			case (A_REPLACE << 8) + A_RM:
			case (A_MODIFY  << 8) + A_RM:
				// Prior replace/modify is irrelevant now
				break;
			case (A_ADD     << 8) + A_REPLACE:
			case (A_RM      << 8) + A_REPLACE:
			case (A_REPLACE << 8) + A_REPLACE:
			case (A_MODIFY  << 8) + A_REPLACE:
				// "Moved to" can unconditionally replace anything
				break;
			case (A_ADD     << 8) + A_MODIFY:
				// Still need to make initial add
			case (A_MODIFY  << 8) + A_MODIFY:
				return;
			default:
				fprintf(stderr, "Not sure how to handle action %d => %d (%d %s)\n", this->action, action, flags, p);
				abort();
			}
			LOG_DEBUG("Change action of %s: %d =%d> %d (flags %d => %d)", p, this->action, actIn, action, this->flags, flags);
			if (action == -1)
			{
				*thisp = this->next;
				free(this->p);
				free(this);
				return;
			}
			this->action = action;
			this->flags = flags;
			return;
		}
	}
	
	// New entry
	LOG_DEBUG("Set action of %s: %d (flags %d)", p, action, flags);
	struct vll *n = malloc(sizeof(struct vll));
	n->p = strdup(p);
	n->action = action;
	n->flags = flags;
	n->next = *top;
	*top = n;
}

static
void
vll_remove(struct vll ** const top, struct vll * const end, const enum action act) {
	for (struct vll **this_p = top; *this_p != end; ) {
		struct vll *this = *this_p;
		const bool remove_this = ((this->action == A_RM) == (act == A_RM));
		if (remove_this) {
			*this_p = this->next;
			free(this->p);
			free(this);
		} else {
			this_p = &this->next;
		}
	}
}

static
void
vll_free(struct vll**top, struct vll *new_first) {
	for (struct vll *next, *this = *top; this != new_first; this = next)
	{
		next = this->next;
		free(this->p);
		free(this);
	}
	*top = new_first;
}

struct cmdbuilder {
	char **cmd;
	char **args;
	size_t count;
	char **dcmd;
};

static inline
bool cmd_do(struct cmdbuilder * const p, const int fd_inotify) {
	if (!read_will_block(fd_inotify)) {
		// Not in a consistent state anymore; go process inotify events first
		LOG_DEBUG("New inotify events%s, abandoning current staging", "");
		return false;
	}
	
	p->args[p->count] = NULL;
	
	int retries = 5;
	while (my_system2(p->cmd[0], p->cmd)) {
		// Command failed
		
		if (!read_will_block(fd_inotify)) {
			// ...probably because of inconsistent state
			LOG_DEBUG("New inotify events%s, abandoning current staging", " (last command failed)");
			return false;
		}
		
		fprintf(stderr, "Runtime add failed with no inotify events; %s\n", (retries ? "retrying" : "giving up"));
		if (!retries--) {
			abort();
		}
	}
	
	return true;
}

static
void
vll_stage_all(struct vll**changes_p, const int fd_inotify) {
	struct vll*changes = *changes_p;
	struct cmdbuilder act[2];
	
	char *add[5 + MAX_ARGS] = {"git", "add", "-A", "--"};
	act[A_ADD].cmd  = add;
	act[A_ADD].args = &add[4];
	act[A_ADD].count= 0;
	
	char *rm[7 + MAX_ARGS] = {"git", "rm", "--ignore-unmatch", "--force", "--cached", "--"};
	act[A_RM].cmd  = rm;
	act[A_RM].args = &rm[6];
	act[A_RM].count= 0;
	
	for (struct vll *next, *this = changes; this; this = next)
	{
		next = this->next;
		
		struct cmdbuilder *c;
		if (this->action == A_RM) {
			c = &act[A_RM];
		} else {
			c = &act[A_ADD];
		}
		
		c->args[c->count++] = this->p;
		if (c->count < MAX_ARGS)
			continue;
		if (!cmd_do(c, fd_inotify)) return;
		c->count = 0;
		
		// We need to update the changes list or things can get confused
		vll_remove(changes_p, next, this->action);
	}
	
	if (act[A_ADD].count) {
		if (!cmd_do(&act[A_ADD], fd_inotify)) return;
		vll_remove(changes_p, NULL, A_ADD);
	}
	
	if (act[A_RM].count) {
		if (!cmd_do(&act[A_RM], fd_inotify)) return;
		vll_free(changes_p, NULL);
	}
}

struct inotify_watch {
	char*path;
	int wd;
	struct inotify_watch*next;
};

static
void
my_add_watch2(struct inotify_watch**top, int fd, const char *pathname, uint32_t mask, int recursive) {
	if (recursive)
	{
		DIR *D = opendir(pathname);
		assert(D || errno == ENOTDIR);
		if (D)
		{
			errno = 0;
			for (struct dirent *DE; DE = readdir(D); )
			{
				if (DE->d_name[0] == '.' && ((DE->d_name[1] == '.' && DE->d_name[2] == '\0') || DE->d_name[1] == '\0'))
					continue;
#ifdef IGNORE_ROOT_GIT
				if (recursive == 1 && !strcmp(".git", DE->d_name))
					continue;
#endif
				if (DE->d_type == DT_DIR)
				{
					// FIXME: these snprintf/sprintf could easily be replaced with strlen etc (inline func?)
					char npn[1 + snprintf(DE->d_name, 0, "%s/%s", pathname, DE->d_name)];
					sprintf(npn, "%s/%s", pathname, DE->d_name);
					my_add_watch2(top, fd, npn, mask, 2);
				}
			}
			assert(!errno);
			closedir(D);
		}
	}
	
	LOG_DEBUG("Adding watch on %s", pathname);
	
	struct inotify_watch *n = malloc(sizeof(struct inotify_watch));
	n->path = strdup(pathname);
	n->wd = inotify_add_watch(fd, pathname, mask);
	
	assert(n->wd != -1);
	
	n->next = *top;
	*top = n;
}
#define my_add_watch(top, fd, path, mask) my_add_watch2(top, fd, path, mask, 0)
#define my_add_watch_recursive(top, fd, path, mask) my_add_watch2(top, fd, path, mask, 1)

static
void
my_rm_watch(struct inotify_watch**top, int fd, int wd) {
	for (struct inotify_watch *i, **ii = top; i = *ii; ii = &i->next)
	{
		if (i->wd == wd)
		{
			*ii = i->next;
			inotify_rm_watch(fd, wd);
			free(i->path);
			free(i);
			return;
		}
	}
	fprintf(stderr, "Tried to remove non-existent watch: %d\n", wd);
	abort();
}

static
const
char*
get_wd_path(struct inotify_watch**top, int fd, int wd) {
	for (struct inotify_watch*i = *top; i; i = i->next)
		if (i->wd == wd)
			return i->path;
	return NULL;
}

int
main(int argc, char**argv) {
	setbuf(stdout, NULL);
	setbuf(stderr, NULL);
	
	assert(argc > 1);
	const char *dir = argv[1];
	
	assert(!chdir(dir));
	LOG_DEBUG("Changed to %s", dir);
	
	int fd_inotify = inotify_init();
	assert(fd_inotify != -1);
	
	struct inotify_watch *wlist = NULL;
	my_add_watch_recursive(&wlist, fd_inotify, ".", MY_WATCH_EVENTS);
	
	// Initial global update check
	{
		int retries = 5;
		while (my_system("git", "add", "--all")) {
			// --ignore-errors doesn't actually ignore errors in current versions of git, so retry a few times
			fprintf(stderr, "Initial global update add failed; %s\n", (retries ? "retrying" : "giving up"));
			if (!retries--) {
				abort();
			}
		}
	}
	my_system("git", "commit", "-m", "Initial global update commit");
	
	struct vll *changes = NULL;
	
	int todo = 3;
	int commit_auto = 0;
	
	char buf[BUFFER_SIZE] __attribute__((aligned(__alignof__(struct inotify_event))));
	ssize_t buf_len = 0;
	
	while (1)
	{
		fd_set rfds;
		FD_ZERO(&rfds);
		FD_SET(fd_inotify, &rfds);
		
		struct timeval tv;
		tv.tv_usec = 0;
		switch (changes ? 1 : todo) {
		case 0:  // nothing
			LOG_DEBUG("Waiting for changes...%s","");
			break;
		case 1:  // add/remove
			tv.tv_sec = 5;
			LOG_DEBUG("Scheduling add/remove in %d seconds...", (int)tv.tv_sec);
			break;
		case 2:  // commit
			tv.tv_sec = 15;
			LOG_DEBUG("Scheduling commit in %d seconds...", (int)tv.tv_sec);
			break;
		case 3:  // push
			tv.tv_sec = 30;
			LOG_DEBUG("Scheduling push in %d seconds...", (int)tv.tv_sec);
			break;
		}
		
		int have_inotify =
		select(fd_inotify + 1, &rfds, NULL, NULL, (changes || todo) ? &tv : NULL);
		
		if (have_inotify)
		{
			ssize_t rrv = read(fd_inotify, buf, sizeof(buf));
			if (rrv == -1)
			{
				perror(NULL);
				exit(1);
			}
			buf_len = rrv;
		}
		
		if (buf_len)
		{
			struct inotify_event *ev;
			for (ssize_t offset = 0; offset < buf_len; offset += sizeof(struct inotify_event) + ev->len)
			{
				ev = (struct inotify_event*)&buf[offset];
				// WARNING: "If successive output inotify events produced on the inotify file descriptor are identical (same wd, mask, cookie, and name) then they are coalesced into a single event."
				
				const char *fullpath = get_wd_path(&wlist, fd_inotify, ev->wd);
				ssize_t fullpath_slen = 0;
				if (ev->len && ev->name[0]) {
					fullpath_slen = snprintf(buf, 0, "%s/%s", fullpath, ev->name) + 1;
					assert(fullpath_slen >= 0);
				}
				char fullpath_s[fullpath_slen];
				if (fullpath_slen)
				{
					sprintf(fullpath_s, "%s/%s", fullpath, ev->name);
					fullpath = fullpath_s;
				}
				
#ifdef DEBUG
				printf("Event:");
				if (ev->mask & IN_ACCESS)
					printf(" Access");
				if (ev->mask & IN_ATTRIB)
					printf(" Attrib");
				if (ev->mask & IN_CLOSE_WRITE)
					printf(" CLOSE_WRITE");
				if (ev->mask & IN_CLOSE_NOWRITE)
					printf(" CLOSE_NOWRITE");
				if (ev->mask & IN_CREATE)
					printf(" Created");
				if (ev->mask & IN_DELETE)
					printf(" Deleted");
				if (ev->mask & IN_DELETE_SELF)
					printf(" Self-deleted");
				if (ev->mask & IN_MODIFY)
					printf(" Modified");
				if (ev->mask & IN_MOVE_SELF)
					printf(" Self-move");
				if (ev->mask & IN_MOVED_FROM)
					printf(" Moved-from");
				if (ev->mask & IN_MOVED_TO)
					printf(" Moved-to");
				if (ev->mask & IN_OPEN)
					printf(" Open");
				if (ev->mask & IN_Q_OVERFLOW)
					printf(" QueueOVERFLOW");
				printf(": %s\n", fullpath);
#endif
				if (ev->mask & (IN_CREATE | IN_MOVED_TO))
				{
					assert(ev->len && ev->name[0]);
					if (ev->mask & IN_ISDIR)
						my_add_watch(&wlist, fd_inotify, fullpath, MY_WATCH_EVENTS);
						// FIXME: only need to add watch immediately before 'git add'
					vll_set_action(&changes, fullpath, (ev->mask & IN_MOVED_TO) ? A_REPLACE : A_ADD, ev->mask);
				}
				if (ev->mask & (IN_MODIFY))
					vll_set_action(&changes, fullpath, A_MODIFY, ev->mask);
				if (ev->mask & (IN_DELETE | IN_MOVED_FROM))
					vll_set_action(&changes, fullpath, A_RM, ev->mask);
				if (ev->mask & IN_DELETE_SELF)
					my_rm_watch(&wlist, fd_inotify, ev->wd);
				if (ev->mask & IN_Q_OVERFLOW)
				{
					fprintf(stderr, "FIXME: this Q_OVERFLOW method will FAIL TO MONITOR NEW DIRECTORIES\n");
					abort();
					commit_auto = 1;
					break;
				}
			}
#ifdef DEBUG
			puts("DONE");
#endif
			buf_len = 0;
		}
		
		if (!have_inotify && (todo = changes ? 1 : todo))
		{
			if (commit_auto)
			{
				my_system("git", "add", "--all");
				todo = 2;
			}
			switch (todo) {
			case 1:
				++todo;
				vll_stage_all(&changes, fd_inotify);
				break;
			case 2:
				++todo;
				my_system("git", "commit", "-m", "_");
				commit_auto = 0;
				break;
			case 3:
				todo = 0;
				my_system("git", "push");
				break;
			}
		}
		
#ifdef CHECKS
		if (!changes && read_will_block(fd_inotify))
		{
			if (!system("git ls-files -omd | grep ."))
			{
				if (read_will_block(fd_inotify))
				{
				fprintf(stderr, "git reports index de-sync!\n");
				abort();
				}
			}
			else
				LOG_DEBUG("git reports index fully in-sync%s","");
		}
#endif
	}
	
}
